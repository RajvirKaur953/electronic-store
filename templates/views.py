from django.shortcuts import render
from django.contrib.auth.models import User
from myapp.models import customer,product,category,cart,order,mail
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
from django.contrib import messages
import math
from django.db.models import Q
import random
import json
import datetime

from django.views.decorators.csrf import csrf_exempt
from django.core.mail import EmailMessage
from django.urls import reverse

@csrf_exempt
def payment_done(request):
    return HttpResponse('Payment Successful')
@csrf_exempt
def payment_canceled(request):
    return HttpResponse('Payment Canceled')


#categories
cat = category.objects.filter(sub_cat=None)


# Create your views here.
def base(request):
    return render(request,'base.html')

def getproduct(request):
    if 'cat' in request.GET:
            c = request.GET['cat']
            catobject = category.objects.get(category_name=c)
            dt = category.objects.filter(sub_cat=catobject).values()
            ab = []
            for i in dt:
                sub_cat = category.objects.get(id=i['id'])
                ab += list(product.objects.filter(product_category=sub_cat).values())
            products = product.objects.filter(product_category=catobject).values()
            allproducts = list(products)
            final = ab+allproducts
            return JsonResponse(final,safe=False)
import json
def singleproduct(request):
    if 'prid' in request.GET:
            id = request.GET['prid']
            singlep =product.objects.get(id=id)
            abcd = {"id":singlep.id,"name":singlep.product_name,"price":str(singlep.price),"sale_price":str(singlep.sale_price),"photo":str(singlep.photo),"description":singlep.description}
            dump = json.dumps(abcd)
            return HttpResponse(dump, content_type='application/json')
    return render(request,'front.html')

def front(request):
    alls = product.objects.all().order_by('-id')[:4]

    if request.method=='GET':
        if 'cat' in request.GET:
            c = request.GET['cat']
            catobject = category.objects.get(category_name=c)
            dt = category.objects.filter(sub_cat=catobject).values()
            ab = []
            for i in dt:
                sub_cat = category.objects.get(id=i['id'])
                ab += list(product.objects.filter(product_category=sub_cat).values())
            products = product.objects.filter(product_category=catobject).values()
            allproducts = list(products)
            final = ab+allproducts
            return JsonResponse(final,safe=False)
    return render(request,'front.html',{'cat':cat,'alls':alls})

def about(request):
    return render(request,'aboutus.html')

def usermail(request):
    if request.method == 'POST':

        name = request.POST['Name']
        email = request.POST['Email']
        phone = request.POST['Telephone']
        msz = request.POST['message']

        message = mail(name=name, email=email, phone=phone,message=msz)
        message.save()
        return render(request,'mailus.html',{'cat':cat,'status':True,'name':name})
    return render(request,'mailus.html',{'cat':cat})



def mobiles(request):
    all = product.objects.all()
    cat = category.objects.filter(sub_cat=None)
    alls = product.objects.all().order_by('-id')[:5]
    
    if 'prid' in request.GET:
            id = request.GET['prid']
            singlep =product.objects.get(id=id)
            abcd = {"id":singlep.id,"name":singlep.product_name,"price":str(singlep.price),"sale_price":str(singlep.sale_price),"photo":str(singlep.photo),"description":singlep.description}
            dump = json.dumps(abcd)
            return HttpResponse(dump, content_type='application/json')

    return render(request,'mobiles.html',{'all':all,'cat':cat,'alls':alls})



@login_required
def dashboard(request):
    return render(request,'dashboard.html')

@login_required
def profile(request):
    user = User.objects.get(username= request.user.username)
    reg = customer.objects.get(user=user)
    return render(request,'myprofile.html',{'data':user,'profile':reg})


def change_profile(request):
    user = User.objects.get(username= request.user.username)
    reg = customer.objects.get(user=user)
    if request.method =="POST":
        us = request.POST['username']
        em = request.POST['email']
        dob = request.POST['dob']
        add = request.POST['address']

        if us !="" or em !="" or dob !="" or add !="" :
            user.username = us
            user.email=em
            user.save()
            reg.address=add
            reg.dob = dob
            reg.save()
            return render(request,'changeprofile.html',{'data':user,'profile':reg,'status':'Changes saved successfully!!','c':'success'})
        else:
            return render(request,'changeprofile.html',{'data':user,'profile':reg,'status':'Changes are not saved!!','c':'danger'})

    return render(request,'changeprofile.html',{'data':user,'profile':reg})


from django.contrib.auth.hashers import check_password
@login_required
def change_password(request):
    user = User.objects.get(username= request.user.username)
    login_user_password = request.user.password
    if request.method == "POST":
        current = request.POST['old']
        newpass = request.POST['new']

        check = check_password(current,login_user_password)
        if check==True:
            user.set_password(newpass)
            user.save()
            # print("Matched!!!")
            return render(request,'changepassword.html',{'status':'Password changed successfully','col':'success'})

        else:
             return render(request,'changepassword.html',{'status':'invalid password','col':'danger'})
    return render(request,'changepassword.html')


def uservalid(request):
    if request.method=='GET':
        if 'username' in request.GET:
            un = request.GET['username']
            check = User.objects.filter(username=un)
            if(len(check)>0):
                return HttpResponse('A user with this name already exists')
            
            else:
                return HttpResponse('Success')

def registration(request):
   
    if request.method=="POST":
        name = request.POST['name']
        em = request.POST['email']
        ps = request.POST['password']
        pic = request.FILES['image']
        gender = request.POST['gender']
        dob = request.POST['dob']       
        
        check = User.objects.filter(username=em)
        if(len(check)>0):
            return render(request,'front.html',{'status':'user with this name already exists!!'})
        
        else:
            
            user = User.objects.create_user(em,em,ps)
            user.first_name=name
            user.save()

            prof=customer(user=user,profile=pic,date_of_birth=dob,gender=gender)
            prof.save()
            return render(request,'front.html',{'status':'Registred Successfully!!'})
    return render(request,'front.html')


def uslogin(request):
    if request.method == 'POST':
        usname = request.POST['email']
        pwd = request.POST['password']
        print('User = ',usname)
        user = authenticate(username=usname,password=pwd)
        if user:
            if user.is_staff:
                login(request,user)
                response= HttpResponseRedirect('/')
                response.set_cookie('username',usname)
                response.set_cookie('id',user.id)
                response.set_cookie('logintime',datetime.datetime.now())
                return response
            elif user.is_active:
                login(request,user)
                response= HttpResponseRedirect('/')
                response.set_cookie('username',usname)
                response.set_cookie('id',user.id)
                response.set_cookie('logintime',datetime.datetime.now())
                return response
            
        else:
            return render(request,'front.html',{'status': 'Invalid User Details'})
    return render(request,'front.html')



def uslogout(request):
    logout(request)
    response = HttpResponseRedirect('/')
    response.delete_cookie('username')
    return response




def addcart(request):
    if not request.user.is_authenticated:
        return render(request,'cart.html',{'status':'You Need To Login First'})
    user = User.objects.get(username=request.user.username)
    viewCart = cart.objects.filter(user_id=user)
    total_cart = []
    if request.method == 'POST':
        print(request.POST)
        item = request.POST['item_id']
        price = request.POST['amount']
        qty = request.POST['add']

        
        check = cart.objects.all()
        pro = product.objects.get(id=item)
        exist = False
        for x in check:
            if x.product_id==pro and x.user_id==user:
                exist=True          
        if exist is True:  
            messages.warning(request,'Item Already in Your Cart')
        else:
            saveCart = cart(user_id=user, product_id=pro, quantity=qty)
            saveCart.save()
            messages.success(request,'Item Added to Your Cart')
    
    if 'q' in request.GET:
        newQ = request.GET['q']
        cartId = request.GET['id']
        getCart = cart.objects.get(id=cartId)
        getCart.quantity = newQ            
        getCart.save() 
        details = {
            'quantity':getCart.quantity,
            'sale_price':getCart.product_id.sale_price,
            'price':getCart.product_id.price,
            'cart_size':len(viewCart),
        }          
        return JsonResponse(details)
    cid = ''
    for x in viewCart:
        cid+=str(x.id)+'/'
        user_cart = {}
        user_cart['id'] = x.id
        user_cart['product_name'] = x.product_id.product_name
        user_cart['photo'] = x.product_id.photo
        user_cart['product_cat'] = x.product_id.product_category
        user_cart['quantity'] = x.quantity
        user_cart['price'] = x.product_id.sale_price
        user_cart['market']= x.product_id.price
        user_cart['offer'] = math.ceil(100-(x.product_id.sale_price/x.product_id.price)*100)
        total_cart.append(user_cart)

    return render(request,'cart.html',{'cart':total_cart,'cart_size':len(total_cart),'cids':cid,'cat':cat,'profile':user})


    
@login_required
def removeCart(request):
    
        
    if request.method=='GET':
        id = request.GET['removeid']
        deleteObj = cart.objects.get(id=id)
        deleteObj.delete()

        #User Cart 
        user = User.objects.get(username=request.user.username)
        viewCart = cart.objects.filter(user_id=user)
        cart_size = len(viewCart)
        data = {
            'msz':'Item Removed Successfully!!!',
            'cart_size':cart_size,
        }
        return JsonResponse(data)
    else:
        messages.info(request, 'You need to login first!!!')
        return render(request,'cart.html')

@login_required
def grandTotal(request):
    user = User.objects.get(username=request.user.username)
    reg = customer.objects.get(user=user)
    viewCart = cart.objects.filter(user_id=user)
    grand_total = 0
    quantity = 0
    for i in viewCart:
        grand_total = grand_total+(i.product_id.sale_price*i.quantity)
        quantity = quantity+i.quantity
        
    data = {
        'grand_total':grand_total,
        'quantity':quantity,
        'email':user.username,
        'name':user.first_name,
        'address':reg.address
    }

    return JsonResponse(data)

def sendMail(request):
    otp = random.randint(100000,999999)
    try:
        name= 'Customer'
        print(otp)
        subject = 'Account Activation'
        if 'name' in request.GET:
            name = request.GET['name']
        message = 'Dear {}, \n {} is your One Time Password for registration \n Thanks for being a part of our organization\n Do not share it with anyone'.format(name,otp)
        receiver = str(request.GET['email'])
        email = EmailMessage(subject,message,to=[receiver,])
        email.send()
        response = ['An OTP Sent to your Email Address @',otp]
        return HttpResponse(response)
    except:
        response = ['OOPs!! Error Occured @',otp]
        return HttpResponse(response)

def my_orders(request):
    user = User.objects.get(username=request.user.username)
    reg= customer.objects.get(user=user)
    ords = order.objects.filter(customer =reg,status='True')
    return render(request,'myorders.html',{'profile':reg,'all_orders':ords,'cat':cat,}) 

@login_required
def get_order(request):
    user = User.objects.get(username=request.user.username)
        
    reg = customer.objects.get(user=user)
    if request.method=='GET':
        dnm = request.GET['dname']
        dem = request.GET['demail']
        dmn = request.GET['dnumber']
        addr = request.GET['daddress']
        amt = request.GET['grandtotal']
        items = request.GET['items']

        order_obj = order(customer=reg,amount=amt,contact_name=dnm,contact_number=dmn,contact_email=dem,delivery_address=addr,cart_id=items)
        order_obj.save()
        oid = order_obj.id

        return HttpResponse(oid)

def order_details(request):
    user= User.objects.get(username=request.user.username)
    reg = customer.objects.get(user=user)
    if request.method=='GET':
        id = request.GET['id']
        orders = order.objects.get(id=id)
        items = orders.cart_id
        all_items = items.split('/')
        products=[]
        c=1
        for i in all_items[:-1]:
            p = cart.objects.get(id=i)
            dict = {
            'sr':c,
            'pname':p.product_id.product_name,
            'price':p.product_id.sale_price,
            'photo':p.product_id.photo,
            'quantity':p.quantity,
            'totalprice':p.quantity*p.product_id.sale_price,

            }
            c+=1
            products.append(dict)
    return render(request,'order_details.html',{'profile':reg,'cat':cat,'products':products,'sz':len(products)}) 

@login_required
def pending_orders(request):
    user= User.objects.get(username=request.user.username)
    reg = customer.objects.get(user=user)
    ords = order.objects.filter(customer =reg,status ='True')
    return render(request,'pending.html',{'profile':reg,'pending_orders':ords,'cat':cat,})

@login_required
def success_payment(request):
    reg = customer.objects.get(user__username=request.user.username)
    if 'ORDERID' in request.GET:
        status = request.GET['STATUS']
        if status == "TXN_FAILURE":
            return HttpResponseRedirect('/')
        order_id = request.GET['ORDERID']
        txn_id = request.GET['TXNID']
        pay_mode = request.GET['PAYMENTMODE']
        bank_name = request.GET['BANKNAME']

        sid = order_id.split('o')[0]
        order_obj = order.objects.get(id=int(sid))
        order_obj.txn_id = txn_id
        order_obj.payment_mode= pay_mode
        order_obj.bank_name = bank_name
        order_obj.status = True
        order_obj.save()

        try:
            all_items = order_obj.cart_id
            if all_items!='' or all_items !=None:
                ls = all_items.split('/')
                for cid in ls[:-1]:
                    citem = cart.objects.get(id=cid)
                    citem.delete()
        except:
            return render(request,'process.html',{'profile':reg,'status':'Something went wrong!!!','cat':cat})

        return render(request,'process.html',{'profile':reg,'txn_id':txn_id,'cat':cat})

