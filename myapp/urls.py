from django.urls import path
from myapp import views
app_name='myapp'

urlpatterns=[
    path('ContactPage/',views.ContactPage,name='ContactPage'),
    path('dashboard/',views.dashboard,name='dash'),
    path('first/',views.first,name='first'),
    path('about/',views.about,name='about'),
    path('login/',views.uslogin,name='login'),
    path('logout/',views.uslogout,name='logout'),
    path('view_profile/',views.view_profile,name='view_profile'),
    path('change_password/',views.change_password,name='change_password'),
    path('change_profile/',views.change_profile,name='change_profile'),
    path('registerView/',views.registerView,name='registerView'),
    path('checkUser/',views.checkUser,name='checkUser'),
    path('mobile/',views.mobile,name='mobile',),
    path('getproduct/',views.getproduct,name='getproduct',),
    path('addcart/',views.addcart,name='addcart',),
    path('forgot_password/',views.forgot_password,name='forgot_password',),
    path('find_user/',views.find_user,name='find_user',),
    path('my_orders/',views.my_orders,name='my_orders',),
    path('order_details/',views.order_details,name='order_details',),
    path('pending_orders/',views.pending_orders,name='pending_orders',),
    path('removeCart/',views.removeCart,name='removeCart',),
    path('grandTotal/',views.grandTotal,name='grandTotal',),
    path('get_order/',views.get_order,name='get_order',),
    path('success_payment/',views.success_payment,name='success_payment',),
    path('sendMail/',views.sendMail,name='sendMail',),
    path('profile/',views.profile,name='profile'),
    path('front/',views.front,name='front'),
    path('singleproduct/',views.singleproduct,name='singleproduct'),

]