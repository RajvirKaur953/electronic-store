from django.shortcuts import render
from django.contrib.auth.models import User
from myapp.models import customer, category, product, cart, order
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponse,HttpResponseRedirect, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import EmailMessage
from django.contrib import messages
import datetime
import json
import random
import math

# Create your views here.
@csrf_exempt
def payment_canceled(request):
    return HttpResponse('Payment Canceled')

def registerView(request):
    if request.method=="POST":
        username=request.POST['name']
        email=request.POST['email']
        password=request.POST['password'] 
        mobile=request.POST['contact']
        if 'image' in request.FILES:
            profile=request.FILES['image']
        address=request.POST['ad']

        user=User.objects.create_user(username,email,password)
        user.save()

        cust=customer(user=user,mobile_no=mobile,address=address,profile_pic=profile)
        cust.save()
        return HttpResponseRedirect('/myapp/login')
def index(request):
    if request.method=='POST':
        if 'sin' in request.POST:
            email=request.POST['email']
            password=request.POST['password'] 

            user=authenticate(email=email,password=password)
            if user:
                if user.is_active:
                    signin(request,user)
                    response=HttpResponseRedirect('/myapp/dash/')
                    if 'rememberme' in request.POST:
                        response.set_cookie('email',email)
                        response.set_cookie('userid',user.id)
                        response.set_cookie('logintime',datetime.datetime_now())
                        return response
            else:
                return render(request,'dashboard.html',{'status':'You are not Registered User!!'})
    return render(request,'Home.html')

def checkUser(request):
    if request.method=='GET':
        un=request.GET['usern']
        ch=User.objects.filter(username=un)
        if len(ch)>=1:
            return HttpResponse('User with this name already exists!!')
        else:
            return HttpResponse('Username Validation Success!!')
        print(ch,len(ch))

@login_required
def dashboard(request):
    log=User.objects.get(username=request.user)
    cust=customer.objects.get(user=log)
    return render(request,'profile.html',{'profile':cust})

def profile(request):
    return render(request,'profile.html')

@login_required
def view_profile(request):
    log=User.objects.get(username=request.user)
    cust=customer.objects.get(user=log)
    return render(request,'my_profile.html',{'profile':cust})

def change_profile(request):
    log=User.objects.get(username=request.user.username)
    cust=customer.objects.get(user=log)
    if request.method=='POST':
        username=request.POST['name']
        email=request.POST['email']
        contact=request.POST['contact']
        address=request.POST['ad']
        log.username=username
        log.email=email
        log.save()
        cust.mobile_no=contact
        cust.address=address
        cust.save()
        if 'image' in request.FILES:
            img=request.FILES['image']
            cust.change_profile=img
            cust.save()
        return render(request,'change_profile.html',{'status':'Changed Successfully!!!','profile':cust})

    return render(request,'change_profile.html',{'profile':cust})

def ContactPage(request):
    return render(request,'ContactPage.html')

def first(request):
    return render(request,'HomePage.html')

def about(request):
    return render(request,'About.html')

def uslogin(request):
    if request.method=='POST':
        if 'sin' in request.POST:
            username=request.POST['username']
            password=request.POST['password'] 
            user=authenticate(username=username,password=password)
            if user:
                if user.is_active:
                    login(request,user)
                    response=HttpResponseRedirect('/myapp/dashboard/')
                    if 'rememberme' in request.POST:
                        response.set_cookie('email',email)
                        response.set_cookie('userid',user.id)
                        response.set_cookie('logintime',datetime.datetime_now())
                    return response
            else:
                return render(request,'Home.html',{'status':'You are not Registered User!!'})
    return render(request,'Login.html')

@login_required
def uslogout(request):
    logout(request)
    response=HttpResponseRedirect('/')
    response.delete_cookie('username')
    response.delete_cookie('userid')
    response.delete_cookie('logintime')
    return response

@login_required
def change_password(request):
    user_obj=User.objects.get(username=request.user.username)
    cust=customer.objects.get(user=user_obj)
    if request.method=='POST':
        old=request.POST['old']
        np=request.POST['new']
        match=check_password(old,user_obj.password)
        if match==True:
            user_obj.set_password(np)
            user_obj.save()
            return render(request,'change_password.html',{'profile':cust,'status':'Password Changed Successfully!!','col':'success'})
        else:
            return render(request,'change_password.html',{'profile':cust,'status1':'Incorrect Password!!','col':'danger'})
    return render(request,'change_password.html',{'profile':cust,})

  

def mobile(request):
    all=product.objects.all().order_by('-id')[:6]
    cat=category.objects.filter(sub_cat=None)
    if 'prid' in request.GET:
        id = request.GET['prid']
        singlep =product.objects.get(id=id)
        abcd = {"id":singlep.id,"name":singlep.product_name,"price":str(singlep.price),"sale_price":str(singlep.sale_price),"photo":str(singlep.photo),"description":singlep.description}
        dump = json.dumps(abcd)
        return HttpResponse(dump, content_type='application/json')
    return render(request,'MobProduct.html',{'all':all,'cat':cat})


#categories
cat = category.objects.filter(sub_cat=None)

from django.http import JsonResponse
def getproduct(request):
    final=[]
    c = request.GET['cat']
    catobject = category.objects.get(category_name=c)
    dt = category.objects.filter(sub_cat=catobject).values()
    products = product.objects.filter(product_category=catobject).values()
    allproducts = list(products)
   
    return JsonResponse(allproducts,safe=False)
def all_cats():
    #All Categories with sub categories
    main=[]
    for i in cat:
        ab = []
        ab.append(i.category_name)
        ab += list(category.objects.filter(sub_cat=i).values())
        main.append(ab) 
    return main  

@login_required
def addcart(request):
    log = User.objects.get(username=request.user.username)
    cust = customer.objects.get(user__username=request.user.username)
    viewCart = cart.objects.filter(user_id=log)
    total_cart = []
    if request.method == 'POST':
        print(request.POST)
        item = request.POST['item_id']
        price = request.POST['amount']
        qty = request.POST['add']


        check = cart.objects.all()
        pro = product.objects.get(id=item)
        exist = False
        for x in check:
            if x.product_id==pro and x.user_id==log:
                exist=True 
        if exist is True: 
            messages.warning(request,'Item Already in Your Cart')
        else:
            saveCart = cart(user_id=log, product_id=pro, quantity=qty)
            saveCart.save()
            messages.success(request,'Item Added to Your Cart')

    if 'q' in request.GET:
        newQ = request.GET['q']
        cartId = request.GET['id']
        getCart = cart.objects.get(id=cartId)
        getCart.quantity = newQ            
        getCart.save() 
        details = {
            'quantity':getCart.quantity,
            'sale_price':getCart.product_id.sale_price,
            'price':getCart.product_id.price,
            'cart_size':len(viewCart),
        }          
        return JsonResponse(details)
    cid = ''
    for x in viewCart:
        cid+=str(x.id)+'/'
        user_cart = {}
        user_cart['id'] = x.id
        user_cart['product_name'] = x.product_id.product_name
        user_cart['photo'] = x.product_id.photo
        user_cart['product_cat'] = x.product_id.product_category
        user_cart['quantity'] = x.quantity
        user_cart['price'] = x.product_id.sale_price
        user_cart['market']= x.product_id.price
        user_cart['offer'] = math.ceil(100-(x.product_id.sale_price/x.product_id.price)*100)
        total_cart.append(user_cart)
    
    return render(request,'cart.html',{'cart':total_cart,'cart_size':len(total_cart),'cids':cid,'cat':cat,'profile':cust})
    

@login_required
def removeCart(request):
        
        if request.method=='GET':
            id = request.GET['removeid']
            deleteObj = cart.objects.get(id=id)
            deleteObj.delete()

            #User Cart 
            log= User.objects.get(username=request.user.username)
            viewCart = cart.objects.filter(user_id=cust)
            cart_size = len(viewCart)
            data = {
                'msz':'Item Removed Successfully!!!',
                'cart_size':cart_size,
            }
            return JsonResponse(data)
        else:
            messages.info(request, 'You need to login first!!!')
            return render(request,'cart.html',)

@login_required
def grandTotal(request):
    log = User.objects.get(username=request.user.username)
    cust = customer.objects.get(user=log)
    viewCart = cart.objects.filter(user_id=log)
    grand_total = 0
    quantity = 0
    for i in viewCart:
        grand_total = grand_total+(i.product_id.sale_price*i.quantity)
        quantity = quantity+i.quantity
        
    data = {
        'grand_total':grand_total,
        'quantity':quantity,
        'email':log.email,
        'name':log.username,
        'address':cust.address
    }

    return JsonResponse(data)

@login_required
def success_payment(request):
    log = User.objects.get(username=request.user.username)
    cust = customer.objects.get(user=log)
    if 'm' in request.GET:
        id = request.GET['id']
        ord = order.objects.get(id=id)
        ord.status = True
        ord.payment_mode = "Cash On Delivery"
        ord.save()
        return render(request,'process.html',{'profile':cust, 'cat':cat,'txn_id':id})
    if 'ORDERID' in request.GET:
        
        status = request.GET['STATUS']
        if status == "TXN_FAILURE":
            return HttpResponseRedirect('/myapp/addcart')
        order_id = request.GET['ORDERID']
        txn_id = request.GET['TXNID']
        pay_mode = request.GET['PAYMENTMODE']
        bank_name = request.GET['BANKNAME']

        sid = order_id.split('o')[0]
        order_obj = order.objects.get(id=int(sid))
        order_obj.txn_id = txn_id
        order_obj.payment_mode= pay_mode
        order_obj.bank_name = bank_name
        order_obj.status = True
        order_obj.save()

        try:
            all_items = order_obj.cart_id
            if all_items!='' or all_items !=None:
                ls = all_items.split('/')
                for cid in ls[:-1]:
                    citem = cart.objects.get(id=cid)
                    citem.ctype=1
                    citem.save()
                    to=request.user.email
                    sub='Confirmation Mail'
                    msg=' Your Oder has been Confirmed'
                    e=EmailMessage(sub,msg,to=[to,])
                    e.send()
        except:
            return render(request,'process.html',{'profile':cust,'status':'Something went wrong!!!','cat':cat})

        return render(request,'process.html',{'profile':cust,'txn_id':txn_id, 'cat':cat})

def sendMail(request):
    otp = random.randint(100000,999999)
    try:
        name= 'Customer'
        print(otp)
        subject = 'Account Activation'
        if 'name' in request.GET:
            name = request.GET['name']
        message = 'Dear {}, \n {} is your One Time Password for registration \n Thanks for being a part of our organization\n Do not share it with anyone'.format(name,otp)
        receiver = str(request.GET['email'])
        email = EmailMessage(subject,message,to=[receiver,])
        email.send()
        response = ['An OTP Sent to your Email Address @',otp]
        return HttpResponse(response)
    except:
        response = ['OOPs!! Error Occured @',otp]
        return HttpResponse(response)

def my_orders(request):
    log= User.objects.get(username=request.user.username)
    cust= customer.objects.get(user=log)
    ords = order.objects.filter(customer =cust,status=True)
    return render(request,'orders.html',{'profile':cust,'all_orders':ords,'cat':cat,})        

@login_required
def get_order(request):
        log = User.objects.get(username=request.user.username)
        cust = customer.objects.get(user=log)
        if request.method=='GET':
            dnm = request.GET['dname']
            dem = request.GET['demail']
            dmn = request.GET['dnumber']
            addr = request.GET['daddress']
            amt = request.GET['grandtotal']
            items = request.GET['items']
            payment = request.GET['pay']
            order_obj = order(customer=cust,amount=amt,contact_name=dnm,contact_number=dmn,contact_email=dem,delivery_address=addr,cart_id=items)
            order_obj.save()
            oid = order_obj.id
            # return HttpResponse(oid)
            if payment=='COD':
                myarr = [1,oid]
                return JsonResponse(myarr,safe=False)
            else:
                arr = [0,oid]
                return JsonResponse(myarr,safe=False)

def order_details(request):
    log= User.objects.get(username=request.user.username)
    cust = customer.objects.get(user=log)
    if request.method=='GET':
        id = request.GET['id']
        orders = order.objects.get(id=id)
        items = orders.cart_id
        all_items = items.split('/')
        products=[]
        c=1
        for i in all_items[:-1]:
            p = cart.objects.get(id=i)
            dict = {
                'sr':c,
                'pname':p.product_id.product_name,
                'price':p.product_id.sale_price,
                'photo':p.product_id.photo,
                'quantity':p.quantity,
                'totalprice':p.quantity*p.product_id.sale_price,
                
            }
            c+=1
            products.append(dict)
    return render(request,'order_details.html',{'profile':cust,'cat':cat,'products':products,'sz':len(products)}) 

@login_required
def pending_orders(request):
    log= User.objects.get(username=request.user.username)
    cust = customer.objects.get(user=log)
    ords = order.objects.filter(customer =cust,status ='True')
    return render(request,'pending.html',{'profile':cust,'pending_orders':ords,'cat':cat,})    

    

def find_user(request):
    if request.method=='GET':
        un=request.GET['usern']
        ch=User.objects.filter(username=un)
        if len(ch)==0:
            return HttpResponse('not')
        else:
            user = User.objects.get(username=un)
            to=user.email
            sub='Password Retrieval'
            rn=random.randint(100000,999999)
            msg='Your OTP to setup new password is '+str(rn)
            e=EmailMessage(sub,msg,to=[to,])
            e.send()
            return HttpResponse(user.email+','+str(rn))

def forgot_password(request):
    if request.method=='POST':
        name=request.POST['username']
        pwd=request.POST['password']
        user_obj=User.objects.get(username=name)
        user_obj.set_password(pwd)
        user_obj.save()
        return render(request,'login.html',{'status':'Password Set, Login Now!!'})

    return render(request,'forgot.html')

def singleproduct(request):
    if 'prid' in request.GET:
            id = request.GET['prid']
            singlep =product.objects.get(id=id)
            abcd = {"id":singlep.id,"name":singlep.product_name,"price":str(singlep.price),"sale_price":str(singlep.sale_price),"photo":str(singlep.photo),"description":singlep.description}
            dump = json.dumps(abcd)
            return HttpResponse(dump, content_type='application/json')
    return render(request,'Home.html')


def front(request):
    alls = product.objects.all().order_by('-id')[:3]    


