from django.contrib import admin
from myapp.models import register, customer, category, product, cart, order
# Register your models here.
admin.site.register(register)
admin.site.register(customer)
admin.site.register(category)
admin.site.register(product)
admin.site.register(cart)
admin.site.register(order)

